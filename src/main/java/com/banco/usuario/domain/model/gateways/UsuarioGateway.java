package com.banco.usuario.domain.model.gateways;

import com.banco.usuario.domain.model.Usuario;

public interface UsuarioGateway {


    Usuario guardar(Usuario usuario);
    void eliminar(long id);
    Usuario buscarPorId(long id);




}
